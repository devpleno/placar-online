$(function () {

  $("#update-score").click(function () {
    const scoreA = $("#score-a").val();
    const scoreB = $("#score-b").val();
    const notify = $("#score-notify:checked").val();
    const dados = { scoreA, scoreB, notify }

    $.post("/admin/match/" + MATCH_INDEX + "/score", dados, function (data) {
      console.log(data)
    })

    return false;
  })

  $("#video-new").click(function () {
    const dados = { video: $("#video-id").val() }
    $.post("/admin/match/" + MATCH_INDEX + "/videos", dados, function (data) { })
  })

  $("#bid-new").click(function () {
    const dados = {
      time: $("#bid-time").val(),
      half: $("#bid-half").val(),
      team: $("#bid-team").val(),
      subject: $("#bid-subject").val(),
      description: $("#bid-description").val()
    }

    $.post("/admin/match/" + MATCH_INDEX + "/bids", dados, function (data) {
      console.log(data)
    })
  })
  
});